package com.shpp.p2p.cs.asadov.assignment3;

import com.shpp.cs.a.console.TextProgram;


import java.util.InputMismatchException;

/**
 * This program start with any positive integer n.
 * Then each term is obtained from the previous term as follows:
 * if the previous term is even, the next term is one half of the previous term;
 * if the previous term is odd, the next term is 3 times the previous term plus 1.
 */
public class Assignment3Part2 extends TextProgram {

    /**
     * This method gets start number from user
     * and makes the sequence according to the rules.
     */
    public void run() {
        int num = getNumber();
        while (num != 1) {
            if (num % 2 != 0) {
                println(num + " is odd so I make 3 n + 1: " + (num = funcOdd(num)));
            } else {
                println(num + " is even so I take half: " + (num = funcEven(num)));
            }
        }
        println("The end.");
    }

    /**
     * This method calculate the next term is 3 times the previous term plus 1
     *
     * @param num previous term
     * @return the next term.
     */
    private int funcOdd(int num) {
        return 3 * num + 1;
    }

    /**
     * This method calculate the next term is one half of the previous term
     *
     * @param num previous term
     * @return the next term
     */
    private int funcEven(int num) {
        return num / 2;
    }

    /**
     * This method asks user for an integer number more then zero.
     * If user inputs wrong value, it asks him to try again while value will be correct.
     *
     * @return quantity of minutes that user inputs
     */
    private int getNumber() {
        while (true) {
            int num = getInt("Enter an integer number more then zero: ");
            if (num > 0)
                return num;
            else
                println("  --Number must be more then zero. Try again!");
        }
    }

    /**
     * This method asks user to input integer value.
     * If user inputs wrong value, it asks him to try again while value will be correct.
     *
     * @param message Message to display
     * @return int number
     */
    private int getInt(String message) {
        while (true) {
            try {
                return readInt(message);
            } catch (InputMismatchException e) {
                println("  --Invalid input! It may be an int number. Try again.");
                readLine();
            }
        }
    }
}