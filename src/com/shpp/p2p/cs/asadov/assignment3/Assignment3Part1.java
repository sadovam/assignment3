package com.shpp.p2p.cs.asadov.assignment3;

import com.shpp.cs.a.console.TextProgram;

import java.util.InputMismatchException;

/**
 * This program watches if you've done enough exercise for a week.
 */
public class Assignment3Part1 extends TextProgram {
    // The number of minutes for exercise for cardiovacular health.
    public static final int CARDIO_DURATION = 30;
    // The number of days for exercise for cardiovacular health.
    public static final int CARDIO_DAYS = 5;
    // The number of minutes for exercise to keep a low blood pressure.
    public static final int BLOOD_DURATION = 40;
    // The number of days for exercise to keep a low blood pressure.
    public static final int BLOOD_DAYS = 3;
    // The number of minutes in a day.
    public static final int MINUTES_IN_DAY = 1440;

    /**
     * This method counts days of doing enough exercise
     * to keep a low blood pressure and for cardiovacular health
     * and print results.
     */
    public void run() {
        // days of doing enough exercise for cardiovacular health.
        int cardio = 0;
        // days of doing enough exercise to keep a low blood pressure.
        int blood = 0;
        for (int day = 1; day <= 7; day++) {
            int minutes = getMinutes(day);
            if (minutes >= CARDIO_DURATION)
                cardio++;
            if (minutes >= BLOOD_DURATION)
                blood++;
        }
        printResult(cardio, blood);
    }

    /**
     * This method asks user how many minutes he did his exercise in day 'day'.
     * If user inputs wrong value, it asks him to try again while value will be correct.
     * @param day number of a day in a week
     * @return quantity of minutes that user inputs
     */
    private int getMinutes(int day) {
        while (true) {
            int minutes = getInt("How many minutes did you do on day " + day + "?");
            if (minutes > 0 && minutes < MINUTES_IN_DAY) {
                return minutes;
            } else {
                println("  --Number of minutes must be between 0 and " + MINUTES_IN_DAY + "! Try again.");
            }
        }
    }

    /**
     * This method asks user to input integer value.
     * If user inputs wrong value, it asks him to try again while value will be correct.
     * @param message Message to display
     * @return int number
     */
    private int getInt(String message) {
        while (true) {
            try {
                return readInt(message);
            } catch (InputMismatchException e) {
                println("  --Invalid input! It must be an int number. Try again.");
                readLine();
            }
        }
    }

    /**
     * This method print results of doing exercise for a week.
     * @param cardio days of doing enough exercise for cardiovacular health.
     * @param blood days of doing enough exercise to keep a low blood pressure.
     */
    private void printResult(int cardio, int blood) {

        println("Cardiovacular health:");
        if (cardio >= CARDIO_DAYS) {
            println("Great job! You've done enough exercise for cardiovacular health.");
        } else {
            int dc = CARDIO_DAYS - cardio;
            println("You needed to train hard for at least " + dc + " more day(s) a week!");
        }

        println("Blood pressure:");
        if (blood >= BLOOD_DAYS) {
            println("Great job! You've done enough exercise to keep a low blood pressure.");
        } else {
            int db = BLOOD_DAYS - blood;
            println("You needed to train hard for at least " + db + " more day(s) a week!");
        }
    }
}