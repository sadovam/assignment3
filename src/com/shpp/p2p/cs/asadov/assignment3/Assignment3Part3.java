package com.shpp.p2p.cs.asadov.assignment3;

import com.shpp.cs.a.console.TextProgram;


import java.util.InputMismatchException;

/**
 * This program raises double base to int power.
 */
public class Assignment3Part3 extends TextProgram {
    /**
     * This method makes interface to use raiseToPower method.
     */
    public void run() {
        while (true) {
            double base = getDouble("Input base: ");
            int exp = getInt("Input exponent: ");
            println("Result: " + raiseToPower(base, exp));
            String answer = readLine("Type 'q' if you want to quit or something else if not.");
            if (answer.equals("q"))
                break;
        }
    }

    /**
     * This method raise double base to int power
     *
     * @param base     double number that may raise to power
     * @param exponent int number that is power
     * @return result
     */
    private double raiseToPower(double base, int exponent) {
        // if exponent 0 result always 1
        if (exponent == 0)
            return 1;

        // if base 0 result always 0 - protect 0 in power -1
        if (base == 0)
            return 0;

        // if exponent < 0 make multiplicative inversion to base
        if (exponent < 0) {
            exponent = -exponent;
            base = 1 / base;
        }

        // calculating result
        double result = 1;
        for (int i = 0; i < exponent; i++)
            result *= base;
        return result;
    }

    /**
     * This method asks user to input integer value.
     * If user inputs wrong value, it asks him to try again while value will be correct.
     *
     * @param message Message to display
     * @return int number
     */
    private int getInt(String message) {
        while (true) {
            try {
                return readInt(message);
            } catch (InputMismatchException e) {
                println("  --Invalid input! It may be an int number. Try again.");
                readLine();
            }
        }
    }

    /**
     * This method asks user to input number value.
     * If user inputs wrong value, it asks him to try again while value will be correct.
     *
     * @param message Message to display
     * @return double number
     */
    private double getDouble(String message) {
        while (true) {
            try {
                return readDouble(message);
            } catch (InputMismatchException e) {
                println("  --Invalid input! It may be a number. Try again.");
                readLine();
            }
        }
    }
}