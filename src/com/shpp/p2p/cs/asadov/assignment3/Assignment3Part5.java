package com.shpp.p2p.cs.asadov.assignment3;

import acm.util.RandomGenerator;
import com.shpp.cs.a.console.TextProgram;


import java.util.InputMismatchException;

/**
 * This program simulate SPb casino game.
 */
public class Assignment3Part5 extends TextProgram {
    /**
     * This method starts game and watch if it ends.
     */
    public void run() {
        int total = 0;
        int counter;
        // start game
        for (counter = 0; total < 20; counter++) {
            // Score for one game iteration
            int gameResult = game();
            println("This game, you earned $" + gameResult);
            total += gameResult;
            println("Your total is $" + total);
        }
        // Print result of game
        println("It took " + counter + " games to earn $20");
    }

    /**
     * This method simulate one game iteration
     * Depends of flip coin result is 2 times the previous result or return result.
     *
     * @return score for one game iteration
     */
    private int game() {
        RandomGenerator rgen = RandomGenerator.getInstance();
        int result = 1;
        while (true) {
            // flip coin
            if (rgen.nextBoolean()) {
                return result;
            }
            result *= 2;
        }
    }
}

