package com.shpp.p2p.cs.asadov.assignment3;

import acm.graphics.GRect;
import com.shpp.cs.a.graphics.WindowProgram;

/**
 * This program draw pyramid of bricks
 * in the bottom of the window.
 * <p>
 * You can change width and height of application window
 * and parameters of bricks and see that happened.
 */
public class Assignment3Part4 extends WindowProgram {

    // The default width and height of the window.
    public static final int APPLICATION_WIDTH = 500;
    public static final int APPLICATION_HEIGHT = 300;

    // Brick width and height
    private static final int BRICK_WIDTH = 40;
    private static final int BRICK_HEIGHT = 15;

    // The number of bricks in bottom row.
    private static final int BRICKS_IN_BASE = 11;

    /**
     * This method calculates start x and y coordinates
     * to draw pyramid and start drawing.
     */
    public void run() {
        double startX = (getWidth() - BRICKS_IN_BASE * BRICK_WIDTH) / 2.0;
        double startY = getHeight() - BRICK_HEIGHT;
        drawPyramid(startX, startY);
    }

    /**
     * This method draw pyramid line by line.
     *
     * @param startX start x coordinate to draw pyramid.
     * @param startY start y coordinate to draw pyramid.
     */
    private void drawPyramid(double startX, double startY) {
        // Height of pyramid is equal BRICKS_IN_BASE
        for (int i = 0; i < BRICKS_IN_BASE; i++) {
            // calculate line start x coordinate and y coordinate and draw line
            drawLineOfBricks(startX + i * BRICK_WIDTH / 2.0,
                    startY - i * BRICK_HEIGHT, i);
        }
    }

    /**
     * This method draw line of bricks.
     *
     * @param lineStartX start x coordinate to draw line.
     * @param lineY      y coordinate to draw line
     * @param num        number of line of bricks.
     */
    private void drawLineOfBricks(double lineStartX, double lineY, int num) {
        // width of line is equal BRICKS_IN_BASE minus number of line from bottom
        for (int i = 0; i < BRICKS_IN_BASE - num; i++) {
            drawBrick(lineStartX + i * BRICK_WIDTH, lineY);
        }
    }

    /**
     * This method draw one brick.
     *
     * @param x x coordinate to draw brick
     * @param y y coordinate to draw brick
     */
    private void drawBrick(double x, double y) {
        GRect box = new GRect(x, y, BRICK_WIDTH, BRICK_HEIGHT);
        add(box);
    }
}