package com.shpp.p2p.cs.asadov.assignment3;

import acm.graphics.GOval;
import acm.util.RandomGenerator;
import com.shpp.cs.a.graphics.WindowProgram;

import java.awt.*;

/**
 * This program draw animation of ovals witch grows from center one by one.
 * Even ovals filled with foreground color.
 * Odd ovals filled with background color.
 * Width, height, position of each oval on every iteration modified by some random values.
 * <p>
 * You can change width and height of application window
 * and various parameters and see that happened.
 */
public class Assignment3Part6 extends WindowProgram {

    // The default width and height of the window.
    public static final int APPLICATION_WIDTH = 800;
    public static final int APPLICATION_HEIGHT = 600;
    // Max diameter of oval
    private static final double MAX_DIAMETER = Math.sqrt(
            APPLICATION_HEIGHT * APPLICATION_HEIGHT + APPLICATION_WIDTH * APPLICATION_WIDTH);

    // Time of animation
    private static final int ANIMATION_TIME = 5000;
    // Number of frames
    private static final int FRAMES_NUMBER = 125;
    // Pause for each frame
    private static final int PAUSE_TIME = ANIMATION_TIME / FRAMES_NUMBER;
    // Number of ovals may be less than frames number
    private static final int OVALS_NUMBER = FRAMES_NUMBER / 2;

    // Increment fo diameter of oval in every iteration
    private static final double DIAMETER_INCREMENT = MAX_DIAMETER *
            7.0 / Math.min(APPLICATION_HEIGHT, APPLICATION_WIDTH);
    // Delta for random changes in oval size and position
    private static final double RND_WIDTH_SCALE = DIAMETER_INCREMENT / 9;
    private static final double RND_HEIGHT_SCALE = DIAMETER_INCREMENT / 9;
    private static final double RND_X_SHIFT = 1 / 2.5;
    private static final double RND_Y_SHIFT = 1 / 2.5;

    // Colors of background and foreground
    private static final Color BG_COLOR = new Color(30, 9, 2);
    private static final Color FG_COLOR = new Color(255, 117, 0);

    RandomGenerator rgen = RandomGenerator.getInstance();

    /**
     * This method set initial parameters
     * and run program.
     */
    public void run() {
        setBackground(BG_COLOR);
        // Start animation in coordinates startX, startY
        drawAnimation(getWidth() / 2.0, getHeight() / 2.0);
        // Clears window after animation.
        removeAll();
    }

    /**
     * This program draw animation consists of FRAME_NUMBERS frames.
     *
     * @param startX x coordinate of start points
     * @param startY y coordinate of start points
     */
    private void drawAnimation(double startX, double startY) {
        GOval[] ovals = new GOval[OVALS_NUMBER];
        for (int i = 0; i < FRAMES_NUMBER; i++) {
            // Create new oval if needs.
            if (i < OVALS_NUMBER) {
                ovals[i] = drawOval(startX, startY, i);
            }
            // Draw new frame.
            drawFrame(i, ovals);
            pause(PAUSE_TIME);
        }
    }

    /**
     * This method makes new frame by drawing ovals from older to newer.
     *
     * @param num   Number of frame
     * @param ovals List of ovals
     */
    private void drawFrame(int num, GOval[] ovals) {
        // Redraw all created ovals.
        for (int i = 0; i < Math.min(num, OVALS_NUMBER); i++) {
            if (ovals[i] == null)
                continue;
            if (resizeAndMoveOval(ovals[i], num - i))
                continue;
            // if oval bigger then window replace it with null
            ovals[i] = null;
        }
    }

    /**
     * This method set new size to oval and move it;
     * return false if oval bigger then window and we not need to redraw it.
     *
     * @param oval Oval to resize and move
     * @param age  Age of oval = number of iteration it exists.
     * @return false if oval bigger then window and we not need to redraw it.
     */
    private boolean resizeAndMoveOval(GOval oval, int age) {
        // Increment of width and height sizes of oval
        double deltaW = DIAMETER_INCREMENT +
                rgen.nextDouble(-RND_WIDTH_SCALE, RND_WIDTH_SCALE);
        double deltaH = DIAMETER_INCREMENT +
                rgen.nextDouble(-RND_HEIGHT_SCALE, RND_HEIGHT_SCALE);
        if (!resizeOval(oval, deltaW * age, deltaH * age)) {
            // if oval bigger then window it removes
            remove(oval);
            return false;
        }
        // Move oval
        moveOval(oval, deltaW, deltaH);
        return true;
    }

    /**
     * This method resize oval
     *
     * @param oval  oval to resize
     * @param sizeW new width
     * @param sizeH new height
     * @return false if oval bigger then window and we not need to redraw it.
     */
    private boolean resizeOval(GOval oval, double sizeW, double sizeH) {
        // If oval bigger then window it is not draws and removes.
        if (sizeW > MAX_DIAMETER && sizeH > MAX_DIAMETER) {
            return false;
        }
        oval.setSize(sizeW, sizeH);
        return true;
    }

    /**
     * This method move oval to center
     * because if you resize oval it shifts to right-bottom corner.
     *
     * @param oval   oval to move
     * @param deltaW how much changed width of oval
     * @param deltaH how much changed height of oval
     */
    private void moveOval(GOval oval, double deltaW, double deltaH) {
        double xPeriod = deltaW * RND_X_SHIFT;
        double yPeriod = deltaH * RND_Y_SHIFT;
        double deltaX = deltaW / 2.0 + rgen.nextDouble(-xPeriod, xPeriod);
        double deltaY = deltaH / 2.0 + rgen.nextDouble(-yPeriod, yPeriod);
        oval.move(-deltaX, -deltaY);
    }

    /**
     * This metod draw oval
     *
     * @param x   x coordinate
     * @param y   y coordinate
     * @param num number of oval in list
     * @return oval
     */
    private GOval drawOval(double x, double y, int num) {
        GOval o = new GOval(x, y, 1, 1);
        add(o);
        o.setFilled(true);
        if (num % 2 == 0)
            o.setColor(FG_COLOR);
        else
            o.setColor(BG_COLOR);
        return o;
    }
}